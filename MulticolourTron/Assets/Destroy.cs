﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

	//IEnumerator co;
	// Use this for initialization
	/*void Start () {
		co = DestroyMe ();
		StartCoroutine (co);
	}*/
	void OnEnable () {
		//co = DestroyMe ();
		StartCoroutine (DestroyMe ());
	}

	private IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds (GameManager.Instance.TimeToFade);
			//Debug.Log (tRef.TimeToFade);
		this.GetComponent<ColorType>().color = TRON_COLORS.NONE;
		this.GetComponent<ColorType>().secondaryColor = TRON_COLORS.NONE;
		this.gameObject.SetActive (false);



	}
}
