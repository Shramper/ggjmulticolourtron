﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : ColorType {

	[SerializeField]
	private float m_growthFactor = 0.1f;
	public GameObject creator;

	// Use this for initialization
	void Start () {

	}

	void OnTriggerEnter(Collider col)
	{
		if(col.CompareTag("Player"))
		{
			if (col.GetComponent<Trail> ().curColor != color && col.gameObject != creator) {
				//Debug.Break ();
				col.GetComponent<Trail> ().LoseLife ();
			}
		}
	}

	// Update is called once per frame
	void LateUpdate () {

		this.transform.localScale = new Vector3(this.transform.localScale.x + m_growthFactor, 
			this.transform.localScale.y + m_growthFactor, 
			this.transform.localScale.z );

	}

}
