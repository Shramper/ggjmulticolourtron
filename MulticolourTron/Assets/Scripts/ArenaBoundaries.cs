﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaBoundaries : MonoBehaviour
{
	public GameObject player_1;
	public GameObject player_2;
	public GameObject player_3;
	public GameObject player_4;

	public AudioClip explosionSFX;

	public Text arenaExitWarning;

	public GameObject explosion;

	public float deathTimerPlayer_1 = 10f;
	public float deathTimerPlayer_2 = 10f;
	public float deathTimerPlayer_3 = 10f;
	public float deathTimerPlayer_4 = 10f;

	public bool player_1_IsInArena;
	public bool player_2_IsInArena;
	public bool player_3_IsInArena;
	public bool player_4_IsInArena;

	void Start()
	{
		player_1 = GameObject.Find ("Player1");
		player_2 = GameObject.Find ("Player2");
		player_3 = GameObject.Find ("Player3");
		player_4 = GameObject.Find ("Player4");
	}

	void Update ()
	{
		if (!GameManager.Instance.isOutOfBounds) {
			if (player_1_IsInArena == false && player_1 != null) {
				arenaExitWarning.text = "PLAYER 1 RETURN TO THE ARENA! SELF-DESTRUCT IN: " + Mathf.Round (deathTimerPlayer_1);
				deathTimerPlayer_1 -= Time.deltaTime;
			}

			if (player_2_IsInArena == false && player_2 != null) {
				arenaExitWarning.text = "PLAYER 2 RETURN TO THE ARENA! SELF-DESTRUCT IN: " + Mathf.Round (deathTimerPlayer_2);
				deathTimerPlayer_2 -= Time.deltaTime;
			}

			if (player_3_IsInArena == false && player_3 != null) {
				arenaExitWarning.text = "PLAYER 3 RETURN TO THE ARENA! SELF-DESTRUCT IN: " + Mathf.Round (deathTimerPlayer_3);
				deathTimerPlayer_3 -= Time.deltaTime;
			}

			if (player_4_IsInArena == false && player_4 != null) {
				arenaExitWarning.text = "PLAYER 4 RETURN TO THE ARENA! SELF-DESTRUCT IN: " + Mathf.Round (deathTimerPlayer_4);
				deathTimerPlayer_4 -= Time.deltaTime;
			}

			if (player_1 != null && deathTimerPlayer_1 <= 0f) {
				GameManager.Instance.isOutOfBounds = true;

				arenaExitWarning.text = "";
				AudioSource audio = GetComponent<AudioSource> ();

				audio.clip = explosionSFX;
				audio.PlayOneShot (explosionSFX);

				Instantiate (explosion, player_1.transform.position, transform.rotation);
				Destroy (player_1.gameObject);
				GameManager.Instance.DeathCount++;
			}

			if (player_2 != null && deathTimerPlayer_2 <= 0f) {
				GameManager.Instance.isOutOfBounds = true;

				arenaExitWarning.text = "";

				AudioSource audio = GetComponent<AudioSource> ();

				audio.clip = explosionSFX;
				audio.PlayOneShot (explosionSFX, 1f);

				Instantiate (explosion, player_2.transform.position, transform.rotation);
				Destroy (player_2.gameObject);

				arenaExitWarning.text = "";
				GameManager.Instance.DeathCount++;
			}

			if (player_3 != null && deathTimerPlayer_3 <= 0f) {
				GameManager.Instance.isOutOfBounds = true;

				arenaExitWarning.text = "";

				AudioSource audio = GetComponent<AudioSource> ();

				audio.clip = explosionSFX;
				audio.PlayOneShot (explosionSFX, 1f);

				Instantiate (explosion, player_3.transform.position, transform.rotation);
				Destroy (player_3.gameObject);
				GameManager.Instance.DeathCount++;
			}

			if (player_4 != null && deathTimerPlayer_4 <= 0f) {
				GameManager.Instance.isOutOfBounds = true;
				arenaExitWarning.text = "";

				AudioSource audio = GetComponent<AudioSource> ();

				audio.clip = explosionSFX;
				audio.PlayOneShot (explosionSFX, 1f);

				Instantiate (explosion, player_4.transform.position, transform.rotation);
				Destroy (player_4.gameObject);

				arenaExitWarning.text = "";
				GameManager.Instance.DeathCount++;
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (!GameManager.Instance.isOutOfBounds) {
			arenaExitWarning.text = "";

			if (player_1 != null && other.name == player_1.name) {
				player_1_IsInArena = true;
				deathTimerPlayer_1 = 10f;
			}

			if (player_2 != null && other.name == player_2.name) {
				player_2_IsInArena = true;
				deathTimerPlayer_2 = 10f;
			}

			if (player_3 != null && other.name == player_3.name) {
				player_3_IsInArena = true;
				deathTimerPlayer_3 = 10f;
			}

			if (player_4 != null && other.name == player_4.name) {
				player_4_IsInArena = true;
				deathTimerPlayer_4 = 10f;
			}
		}
	}

	void OnTriggerExit (Collider other)
	{
		if (!GameManager.Instance.isOutOfBounds) {
			if (player_1 != null && other.name == player_1.name)
				player_1_IsInArena = false;

			if (player_2 != null && other.name == player_2.name)
				player_2_IsInArena = false;

			if (player_3 != null && other.name == player_3.name)
				player_3_IsInArena = false;

			if (player_4 != null && other.name == player_4.name)
				player_4_IsInArena = false;
		}
	}
}