﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCountdown : MonoBehaviour {

	private Main mainScript;

	public AudioClip clip3;
	public AudioClip clip2;
	public AudioClip clip1;
	public AudioClip clipGO;

	public AudioSource audioCountdown;

	public void Start() {

		audioCountdown = this.GetComponent<AudioSource>();

	}

	public void setCountdown() {

		mainScript = GameObject.Find ("GameManager").GetComponent<Main> ();
		//mainScript.countdownMax = true;
		GameManager.Instance.countdownMax = true;

	}

	public void countdown3() {

		Debug.Log ("3");
		audioCountdown.clip = clip3;
		audioCountdown.Play ();

	}

	public void countdown2() {

		Debug.Log ("2");
		audioCountdown.clip = clip2;
		audioCountdown.Play ();

	}

	public void countdown1() {

		Debug.Log ("1");
		audioCountdown.clip = clip1;
		audioCountdown.Play ();

	}

	public void countdownStart() {

		Debug.Log ("GO");
		audioCountdown.clip = clipGO;
		audioCountdown.Play ();

	}

}
