﻿using UnityEngine;
using System.Collections;
using Rewired;
[RequireComponent(typeof (CharacterController))]
public class PlayerControllerRotation : MonoBehaviour
{
	public float speed = 5f;
	public float rotateSpeed = 100f;

	Rewired.Player player;

	[SerializeField] int playerID = 0;

	private Rigidbody rb;

	private Main mainScript;

	public GameObject indicator;
	public GameObject indicatorGO;

	float indicOffsetZ = 1.0f;
	float indicOffsetX = 0.5f;
	Vector3 indicPos;

	Animator playerAnim;
	public bool boost;
	public GameObject bomb;
	public Trail trailRef;

	void Start ()
	{
		//mainScript = GameObject.Find ("GameManager").GetComponent<Main> ();
		trailRef = GetComponent<Trail>();
		player = ReInput.players.GetPlayer (playerID);
		rb = GetComponent<Rigidbody> ();

		playerAnim = GetComponent<Animator> ();

		//Setting Indicator Positioning

		if (playerID == 0) {

			indicPos = new Vector3 (this.transform.position.x + indicOffsetX, this.transform.position.y, this.transform.position.z + indicOffsetZ);

			indicatorGO = Instantiate (indicator, indicPos, Quaternion.identity) as GameObject;
			indicatorGO.transform.rotation = this.transform.rotation;

		} else if (playerID == 1) { //dealing with flipping of sprite

			indicPos = new Vector3 (this.transform.position.x - indicOffsetX, this.transform.position.y, this.transform.position.z + indicOffsetZ);

			indicatorGO = Instantiate (indicator, indicPos, Quaternion.identity) as GameObject;
			indicatorGO.transform.rotation = this.transform.rotation; //Need to affect the Z Rotation somehow

			Vector3 rot = indicatorGO.transform.rotation.eulerAngles;
			rot = new Vector3 (rot.x, rot.y, rot.z + 180);
			indicatorGO.transform.rotation = Quaternion.Euler (rot);

		} else if (playerID == 2) {

			indicPos = new Vector3 (this.transform.position.x - indicOffsetX, this.transform.position.y, this.transform.position.z + indicOffsetZ);

			indicatorGO = Instantiate (indicator, indicPos, Quaternion.identity) as GameObject;
			indicatorGO.transform.rotation = this.transform.rotation; //Need to affect the Z Rotation somehow

		} else if (playerID == 3) { //dealing with flipping of sprite

			indicPos = new Vector3 (this.transform.position.x + indicOffsetX, this.transform.position.y, this.transform.position.z + indicOffsetZ);

			indicatorGO = Instantiate (indicator, indicPos, Quaternion.identity) as GameObject;
			indicatorGO.transform.rotation = this.transform.rotation; //Need to affect the Z Rotation somehow

			Vector3 rot = indicatorGO.transform.rotation.eulerAngles;
			rot = new Vector3 (rot.x, rot.y + 180, rot.z);
			indicatorGO.transform.rotation = Quaternion.Euler (rot);

		}

	}

	void Update ()
	{
		playerAnim.SetBool ("Boost", boost);

		if (GameManager.Instance.countdownMax == true) { //When Countdown is done, movement will be available.

			Vector3 movement = transform.right * speed;
			rb.velocity = movement;

			//Debug.Log (this.transform.rotation);

			if (player.GetAxis ("Horizontal") != 0 || player.GetAxis ("Vertical") != 0) {
				float rotateHorizontal = player.GetAxis ("Horizontal");
				float rotateVertical = player.GetAxis ("Vertical");
				float targetAngle = Mathf.Atan2 (rotateVertical, rotateHorizontal) * Mathf.Rad2Deg;

				this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.Euler (90, 0, targetAngle), Time.deltaTime * rotateSpeed);
			}

			//rb.transform.Rotate (0, rotateSpeed * rotateHorizontal * Time.deltaTime, 0, Space.World);

			if (player.GetButton ("Accelerate") && player.GetButton ("Brake"))
				return;

			if (player.GetButtonDown ("Accelerate"))
				speed += 5f;
			
			if (player.GetButtonDown ("Brake"))
				speed -= 5f;
			if (player.GetButtonDown ("Bomb"))
				DropBomb ();

			//if (player.GetButton ("Brake"))
			//	speed -= 1f;

			if (speed <= 5f)
				speed = 5f;

			if (speed >= 35f)
				speed = 35f;

			if (speed >= 20f)
				boost = true;
				//playerAnim.Play ("playerBoost");

			if (speed <= 20f)
				boost = false;
				//playerAnim.Play ("playerMove");

			//if (speed <= 10f && player.GetButton ("Brake"))
			//	speed = 5f;

			//Keeping indicator aligned with player
			if (indicatorGO != null) {
				indicPos = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z + indicOffsetZ); //Storing location calculated from player position

				indicatorGO.transform.position = indicPos;
			}

		}
	}

	private void DropBomb()
	{
		
		if (GameManager.Instance.isBombMode && GameManager.Instance.bombHolder == this.gameObject) {
			GameObject tempBomb = Instantiate (bomb, this.transform.position, Quaternion.Euler(90.0f, 0.0f, 0.0f))as GameObject;
			tempBomb.GetComponent<Bomb> ().creator = this.gameObject;
			tempBomb.GetComponent<Bomb> ().color = trailRef.curColor;
			tempBomb.GetComponent<Bomb> ().secondaryColor = trailRef.curColor;
			tempBomb.GetComponent<SpriteRenderer> ().color = trailRef.colors [(int)trailRef.curColor];
		}

	}
}