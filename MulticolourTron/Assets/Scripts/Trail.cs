﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour {

	/*public GameObject plotPointObject;
	public int numberOfPoints= 50;
	public float animSpeed =1.0f;
	public float scaleInputRange = 2*Mathf.PI; // scale number from [0 to 99] to [0 to 2Pi]
	public float scaleResult = 1.0f;
	public bool animate = true;
	public float t = 0.1f;
	*/
	//private Vector3 m_centerPos;
	private float m_degrees = 90.0f;
	[SerializeField]
	private float m_speed = 1.0f;
	[SerializeField]
	private float m_period = 1.0f;
	[SerializeField]
	private float m_amplitude;
	GameObject[] plotPoints;

	private Vector3 Waypoints;
	private int i;
	private Vector3 TargetPos;
	private Vector3 StartPos;
	[SerializeField]
	private TrailRenderer trail;

	public float TimeToSwitch;

	public Color[] colors;

	public TRON_COLORS curColor = TRON_COLORS.MAGENTA;

	public TRON_COLORS prevColor = TRON_COLORS.MAGENTA;

	public TRON_COLORS storeColor = TRON_COLORS.NONE;

	public GameObject Mover;
	public GameObject thing;
	public Material mat;

	public float TimeToFade;
	[SerializeField]
	private Vector3 offset = Vector3.zero;
	public GameObject col;

	public List<GameObject> CollisionPool;
	public int m_poolNumber = 200;
	public GameObject child;
	public Rigidbody rbRef;
	public PlayerControllerRotation pcRef;
	public SpriteRenderer spRef;

	public int count = 0;
	public bool isChanging = false;
	private IEnumerator changingCo;

	Vector3 CollisionOffset;

	public GameObject arena;

	public SphereCollider colRef;
	//public AudioClip explosionSFX;

	//public GameManager.TRON_COLORS

	void Awake () {

		arena = GameObject.FindGameObjectWithTag ("Arena");
		changingCo = ChangeColorDelay ();
		CollisionOffset = -this.transform.right * 2;
		spRef = this.GetComponent<SpriteRenderer> ();
		pcRef = this.GetComponent<PlayerControllerRotation> (); 
		rbRef = this.GetComponent<Rigidbody> ();
		thing = new GameObject ();
		colors = new Color[7];
		colors [0] = Color.red;
		colors [1] = new Color32 (255, 120, 12, 255);
		colors [2] = Color.yellow;
		colors [3] = Color.green;
		colors [4] = new Color(202/255, 255/255, 255/255);
		colors [5] = Color.blue;
		colors [6] = new Color32(156, 28, 200, 255);
		//m_centerPos = this.transform.position;
		trail = this.GetComponentInChildren<TrailRenderer> ();
		trail.startWidth = .2f;
		trail.endWidth = .2f;
		trail.startColor = colors [(int)curColor];
		trail.endColor = colors [(int)curColor];
		trail.time = TimeToFade;
		SetStats ();
		//Mover.transform.position = this.transform.position;

		//StartCoroutine (ColorSwitch ());
		//StartCoroutine (DropColliders ());

		CollisionPool = new List<GameObject> ();
		for (int i = 0; i < m_poolNumber; i++) {

			GameObject obj = Instantiate (col) as GameObject;
			obj.SetActive (false);
			CollisionPool.Add (obj);

		}

		StartCoroutine (DropColliders());
		//if (plotPointObject == null) //if user did not fill in a game object to use for the plot points
		//	plotPointObject = GameObject.CreatePrimitive(PrimitiveType.Sphere); //create a sphere

		//plotPoints = new GameObject[numberOfPoints]; //creat an array of 100 points.

		//for (int i = 0; i < numberOfPoints; i++)
		//{
		//	plotPoints[i] = (GameObject)GameObject.Instantiate(plotPointObject, new Vector3(i - (numberOfPoints/2), 0, 0), Quaternion.identity); //this specifies what object to create, where to place it and how to orient it
		//}
		//we now have an array of 100 points- your should see them in the hierarchy when you hit play
		//plotPointObject.SetActive(false); //hide the original

	}

	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag ("Barrier")) {
			if (col.GetComponent<ColorType> ().color == curColor || col.GetComponent<ColorType>().secondaryColor == curColor ) {
				//Debug.Log ("No Collision");
			}else if(col.GetComponent<ColorType>().lifetime < 50 && col.GetComponent<ColorType>().player == this.gameObject)
			{
				Debug.Log ("Self Collison");
			}else {
				Debug.Log ("Collision");
				//Debug.Break ();
				Debug.Log(col.GetComponent<ColorType>().color);
				Debug.Log (this.gameObject.name);
				Debug.Log(col.gameObject.name);
				//Debug.Break ();
				LoseLife ();
				//Debug.Break ();
			}
		}
	}

	public void LoseLife()
	{
		if(count == 0)
		{
			GameManager.Instance.DeathCount++;
			trail.transform.parent = this.transform.parent;

			AudioSource audio = arena.GetComponent<AudioSource> ();

			audio.clip = arena.GetComponent<ArenaBoundaries>().explosionSFX;
			audio.PlayOneShot ( arena.GetComponent<ArenaBoundaries>().explosionSFX);

			Instantiate ( arena.GetComponent<ArenaBoundaries>().explosion, this.transform.position, transform.rotation);
			Camera.main.GetComponentInParent<DynamicCamera> ().Remove_Player (this.gameObject);
			Destroy (this.gameObject);
			count++;
		}


	}

	private IEnumerator ColorSwitch()
	{
		while (true) {
			yield return new WaitForSeconds (TimeToSwitch);
			curColor++;
			if ((int)curColor >= colors.Length)
				curColor = (TRON_COLORS)0;



			trail.transform.parent = this.transform.parent;
			thing = new GameObject ();





			Mover = thing;
			//Mover.transform.Rotate (new Vector3 (0, 0, -90));
			//Mover.transform.localPosition = new Vector3 ( this.transform.position.x, this.transform.position.y,  this.transform.position.z);
			Mover.transform.localPosition = this.transform.localPosition -  (offset)/ (m_amplitude*4 );
			Mover.transform.position = new Vector3 (Mover.transform.position.x, -1.0f, Mover.transform.position.z);

			trail = thing.gameObject.AddComponent<TrailRenderer> ();
			trail.time = TimeToFade;
			trail.material = mat;
			if(curColor==0)
				trail.endColor = colors [(int)prevColor];
			else
				trail.endColor = colors [(int)prevColor];
			trail.startColor = colors [(int)curColor];
			trail.startWidth = .2f;
			trail.endWidth = .2f;

			SetStats ();


			thing.transform.parent = child.transform;
		}
	}

	private IEnumerator DropColliders()
	{
		yield return new WaitForSeconds (4.2f);
		while (true) {
			yield return new WaitForSeconds (0.01f);
			{
				//GameObject temp = Instantiate (col, this.transform.position, Quaternion.identity) as GameObject;
				for (int i = 0; i < CollisionPool.Count; i++) {
					if (!CollisionPool [i].activeInHierarchy) {
						//Debug.Break ();


						//CollisionPool [i].transform.position = this.transform.position;
						//CollisionPool [i].transform.localPosition += -this.transform.right;

						CollisionPool [i].transform.position =new Vector3( Mover.transform.position.x, 0, Mover.transform.position.z);

						CollisionPool [i].SetActive (true);
						CollisionPool [i].GetComponent<ColorType> ().color = curColor;
						CollisionPool [i].GetComponent<ColorType> ().lifetime = 0;
						CollisionPool [i].GetComponent<ColorType> ().player = this.gameObject;
						//Debug.Log (isChanging);
						if (isChanging) {
							CollisionPool [i].GetComponent<ColorType> ().secondaryColor = storeColor;
						} else {
							CollisionPool [i].GetComponent<ColorType> ().secondaryColor = TRON_COLORS.NONE;
						}
						//Physics.IgnoreCollision(colRef, CollisionPool[i].GetComponent<SphereCollider>()); 
						//CollisionPool [i].GetComponent<ColorType> ().ReactivateCollison (this.GetComponent<SphereCollider>(), CollisionPool[i].GetComponent<SphereCollider>());
						break;
					}
				}
			}
		}
	}

	private void SetStats()
	{
		switch (curColor) {
		case TRON_COLORS.RED:
			this.m_period = 0.5f;
			this.m_amplitude = 0.1f;
			spRef.color = colors [0];
			break;
		case TRON_COLORS.ORANGE:
			this.m_period =  0.5f;
			this.m_amplitude = 0.09f;
			spRef.color = colors [1];
			break;
		case TRON_COLORS.YELLOW:
			this.m_period =  0.5f;
			this.m_amplitude = 0.08f;
			spRef.color = colors [2];
			break;
		case TRON_COLORS.GREEN:
			this.m_period =  0.5f;
			this.m_amplitude = 0.07f;
			spRef.color = colors [3];
			break;
		case TRON_COLORS.BLUE:
			this.m_period =  0.5f;
			this.m_amplitude = 0.06f;
			spRef.color = colors [4];
			break;
		case TRON_COLORS.INDIGO:
			this.m_period =  0.5f;
			this.m_amplitude = 0.05f;
			spRef.color = colors [5];
			break;
		case TRON_COLORS.MAGENTA:
			this.m_period = 0.5f;
			this.m_amplitude = 0.03f;
			spRef.color = colors [6];
			break;
		}
	}

	float ComputeFunction(float x)
	{
		return Mathf.Sin(x);
	}

	private void SetColorBasedOnSpeed()
	{
		//Debug.Log (rbRef.velocity.magnitude);
		if (pcRef.speed >= 35) {
			prevColor = curColor;
			curColor = TRON_COLORS.RED;
		} else if (pcRef.speed < 35 && pcRef.speed >= 30) {
			prevColor = curColor;
			curColor = TRON_COLORS.ORANGE;
		}else if(pcRef.speed < 30 && pcRef.speed >= 25){
			prevColor = curColor;
			curColor = TRON_COLORS.YELLOW;
		}else if(pcRef.speed < 25 && pcRef.speed >= 20){
			prevColor = curColor;
			curColor = TRON_COLORS.GREEN;
		}else if(pcRef.speed < 20 && pcRef.speed >= 15){
			prevColor = curColor;
			curColor = TRON_COLORS.BLUE;
		}else if(pcRef.speed < 15 && pcRef.speed >= 10){
			prevColor = curColor;
			curColor = TRON_COLORS.INDIGO;
		}else if(pcRef.speed < 10 ){
			prevColor = curColor;
			curColor = TRON_COLORS.MAGENTA;
		}
	}

	public void NewTrail()
	{
		SetColorBasedOnSpeed ();
		count = 0;
		trail.transform.parent = this.transform.parent;
		thing = new GameObject ();


		trail.enabled = false;


		Mover = thing;
		//Mover.transform.Rotate (new Vector3 (0, 0, -90));
		Mover.transform.localPosition = new Vector3 ( this.transform.position.x, this.transform.position.y,  this.transform.position.z);
		Mover.transform.localPosition = this.transform.localPosition -  (offset)/ (m_amplitude*4 );
		Mover.transform.position = new Vector3 (Mover.transform.position.x, -1.0f, Mover.transform.position.z);



		trail = Mover.gameObject.AddComponent<TrailRenderer> ();
		trail.enabled = true;
		trail.time = TimeToFade;
		trail.material = mat;
		if(curColor==0)
			trail.endColor = colors [(int)prevColor];
		else
			trail.endColor = colors [(int)prevColor];
		trail.startColor = colors [(int)curColor];
		trail.startWidth = .2f;
		trail.endWidth = .2f;

		SetStats ();

		thing.transform.parent = child.transform;



	}

	private IEnumerator ChangeColorDelay()
	{
		while (true) {
			Debug.Log (isChanging);
			yield return new WaitForSeconds (1f);

			isChanging = false;
			yield break;
		}
		//StopCoroutine (changingCo);

	}

	private void ChangeColor()
	{
		GameManager.Instance.AllLines.Add (trail);
		trail.transform.parent = this.transform.parent;
		thing = new GameObject ();





		Mover = thing;
		Mover.transform.localPosition = this.transform.localPosition;
	
		offset = Vector3.zero;
		m_degrees = 90;

		Mover.transform.position = new Vector3 (Mover.transform.position.x, -1.0f, Mover.transform.position.z);

		//Destroy (trail);

		trail = thing.gameObject.AddComponent<TrailRenderer> ();
		trail.time = TimeToFade;
		trail.material = mat;
	
		//trail.endColor = colors [(int)prevColor];
		Gradient gradColor = new Gradient ();
		GradientColorKey[] gck = new GradientColorKey[3];
		GradientAlphaKey[] gak = new GradientAlphaKey[3];
		gak [0].alpha = 255;
		gck [0].color =	(colors [(int)prevColor]);
		gck [0].time = 1.0f;
		gak [1].alpha = 255;
		gck [1].color = (colors [(int)curColor]);
		gck [1].time = .62f;
		gak [2].alpha = 255;
		gck [2].color = (colors [(int)curColor]);
		gck [2].time = 0.0f;
		//gradColor.colorKeys[0] = gck;
		//gck = new GradientColorKey (colors [(int)curColor], .25f);
		//gradColor.colorKeys[1] = gck;
		//gck = new GradientColorKey (colors [(int)curColor], 1f);
		//gradColor.colorKeys[2] = gck;
		gradColor.SetKeys(gck,gak);
		//gradColor.colorKeys[1] = colors [(int)curColor];
		//gradColor.colorKeys[2] = colors [(int)curColor];
		trail.colorGradient = gradColor;

		//trail.startColor = colors [(int)curColor];
		this.storeColor = prevColor;
		trail.startWidth = .2f;
		trail.endWidth = .2f;

		SetStats ();


		thing.transform.parent = child.transform;

		this.isChanging = true;
		StopCoroutine (changingCo);
		StartCoroutine (ChangeColorDelay());
	}


	// Update is called once per frame
	void Update()
	{
		
		SetColorBasedOnSpeed ();
		if(curColor != prevColor)
			ChangeColor ();
		//this.transform.rotation = this.transform.parent.rotation;
		float deltaTime = Time.deltaTime;

		//m_centerPos.x += deltaTime * m_speed;

		float degreesPerSecond = 360.0f / m_period;
		m_degrees = Mathf.Repeat (m_degrees + (deltaTime * degreesPerSecond), 360.0f);
		float radians = m_degrees * Mathf.Deg2Rad;
		//Debug.Log (radians);
		offset = new Vector3 ( m_amplitude * -Mathf.Sin (radians), 0,0) /2;
		//Debug.Log (offset);
		Mover.transform.localPosition =  Mover.transform.localPosition - (offset);

	}
		
}
