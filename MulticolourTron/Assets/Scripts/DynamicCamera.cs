﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DynamicCamera : MonoBehaviour {

	public float dampTime = 0.2f; //time for camera to move in position, delay
	public float screenEdgeBuff = 4f; //adds to size so that objects don't goo off camera
	public float minSize = 6.5f;
	public List<Transform> players;
	//public Transform[] players; //targets to be followed by camera

	private Camera mainCam;
	private float zoomSpeed;
	private Vector3 moveVelocity;
	[SerializeField]
	private Vector3 centrePos; //Where the camera is trying to reach, average between targets

	private Main mainScript;

	private void Awake() {

		//mainScript = GameObject.Find ("GameManager").GetComponent<Main> ();
		mainCam = GetComponentInChildren<Camera> ();

	}

	// Update is called once per frame
	void FixedUpdate () {


		if (GameManager.Instance.countdownMax == true) { //When Countdown is done, Camera can function

			Move(); //Run Move function
			Zoom(); //Run Zoom function

		}

	}

	private void Move() {

		FindAveragePosition ();

		transform.position = Vector3.SmoothDamp (transform.position, centrePos, ref moveVelocity, dampTime); //moving position of camera equal to it's current position, desired position, and referencing the speed and dampness of velocity

	}

	private void FindAveragePosition() {

		Vector3 averagePos = new Vector3 ();
		int numPlayers = 0;
		/*
		for (int i = 0; i < GameManager.Instance.Players.Count; i++) { //Get's the total length of the players in the array

			if (GameManager.Instance.Players[i] != null && !GameManager.Instance.Players [i].gameObject.activeSelf) //If the player is not active(dead), it will ignore it
				continue;

			averagePos += GameManager.Instance.Players[i].transform.position; //adds up the player's positions
			numPlayers++;

		}*/

		foreach (Transform player in players) {
			if (player != null) {
				if (!player.gameObject.activeSelf)
					continue;
			
				averagePos += player.transform.position; //adds up the player's positions
				numPlayers++;
			}
		}

		if (numPlayers > 0) { //if we have a player or more in game

			averagePos /= numPlayers; //dividing the average position by how many players

			averagePos.y = transform.position.y; //safety measure, players don't move up and down in y, so neither should camera

			centrePos = averagePos;

		}
		//Debug.Log (numPlayers);

	}

	private void Zoom() {

		float requiredSize = FindRequiredSize ();
		mainCam.orthographicSize = Mathf.SmoothDamp (mainCam.orthographicSize, requiredSize, ref zoomSpeed, dampTime); //zoom out based on the cameras current size and it's required size, referencing speed and dampness.

	}

	private float FindRequiredSize() //SUPER COMPLICATED, LOOK AT TANKS TUTORIAL CAMERA CONTROL VIDEO FOR MORE INFO
	{

		Vector3 desiredLocalPos = transform.InverseTransformPoint (centrePos); //Finding desired position in camera rig's local space

		float size = 0f;

		for (int i = 0; i < players.Count; i++) { //looping through targets to get their local positions

			if(players[i] != null)
			{
				if (!players [i].gameObject.activeSelf) //If the player is not active(dead), it will ignore it
					continue;

				Vector3 playerLocalPos = transform.InverseTransformPoint (players[i].position); //Getting localposition of each player in array
				Vector3 desiredPosToPlayer = playerLocalPos - desiredLocalPos; //finding camera rig's position to the tanks position

				size = Mathf.Max (size, Mathf.Abs (desiredPosToPlayer.y)); //Calculating whether the current y size, or the camera rig's y size is bigger
				size = Mathf.Max (size, Mathf.Abs (desiredPosToPlayer.x) / mainCam.aspect); //Calculating whether the current z size, or the camera rig's z size divided by the camera's aspect is bigger
			}
		}

		size += screenEdgeBuff; //adding buffer for extra distance so players dont go offscreen

		size = Mathf.Max (size, minSize); //make sure we're not too zoomed in by applying minimum size

		return size;

	}

	public void SetStartPositionAndSize() {

		FindAveragePosition ();

		transform.position = centrePos;
		mainCam.orthographicSize = FindRequiredSize ();

	}


	public void Remove_Player(GameObject player)
	{
		players.Remove (player.transform);
	}
}
