﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class Main : MonoBehaviour {

	public bool countdownMax = false;
	public GameObject countdownImage;

	public int controllerCount;

	// Use this for initialization
	void Start () {
		countdownImage = GameObject.FindGameObjectWithTag ("Number");
		//Debug.Break ();
		Rewired_Setup();
	}
		
	private void Rewired_Setup ()
	{
		//Rewired Setup
		//find how many controllers are plugged in
		controllerCount = 0;
		foreach (Rewired.Controller cont in Rewired.ReInput.controllers.Controllers) {
			if (cont.type.ToString() == "Joystick") {
				Debug.Log (cont.name);
				controllerCount++;

			}
		}

		if (controllerCount == 1) {
			controllerCount = 2;
		}

		//turn off the appropriate players using a Regular Expression Match
		foreach (GameObject player in GameManager.Instance.Players) {
			Debug.Log (player.name);
			string match = Regex.Match (player.name, @"\d").ToString();
			Debug.Log (match.ToString ());
			if (int.Parse(match) > controllerCount) {
				player.SetActive (false);
			}
		}

		//Redifine the GameManager
		GameManager.Instance.Players.Clear ();
		GameObject[] temp = GameObject.FindGameObjectsWithTag ("Player");
		foreach (GameObject p in temp) {
			GameManager.Instance.Players.Add (p);
		}

	}
	// Update is called once per frame
	void Update () {

		/*(Debug.Log ();
		if(!GameManager.Instance.isGameRunning && countdownMax) {
			GameManager.Instance.isGameRunning = true;
			countdownImage.SetActive (false);

		}*/

	}

}
