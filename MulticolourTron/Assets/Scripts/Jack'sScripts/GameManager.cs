﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public enum TRON_COLORS
{
	RED,
	ORANGE,
	YELLOW,
	GREEN,
	BLUE,
	INDIGO,
	MAGENTA,
	NONE

}

public class GameManager : Singleton<GameManager> {

	public bool isGameRunning = false;
	public bool isColliding = false;
	public bool isOutOfBounds = false;
	public bool isBombMode = false;

	public GameObject bombHolder;

	public Main mRef;
	public GameObject countdownImage;
	public bool countdownMax = false;

	public GameObject Player1;
	public GameObject Player2;
	public GameObject Player3;
	public GameObject Player4;

	/*public GameObject playerPanel1;
	public GameObject playerPanel2;
	public GameObject playerPanel3;
	public GameObject playerPanel4;
	*/
	private Vector3 Player1Vec;
	private Vector3 Player2Vec;
	private Vector3 Player3Vec;
	private Vector3 Player4Vec;

	/*
	public int p1Lives = 3;
	public int p2Lives = 3;
	public int p3Lives = 3;
	public int p4Lives = 3;

	public Image[] p1Pics;
	public Image[] p2Pics;
	public Image[] p3Pics;
	public Image[] p4Pics;
	
	*/
	private int controllerCount = 0;

	public int DeathCount = 0;

	public DynamicCamera dcRef;

	public List<GameObject> Players;

	public float TimeToFade;

	public List<TrailRenderer> AllLines;

	protected GameManager() { }

	void OnLevelWasLoaded()
	{
		
			AllLines.Clear ();
			Players.Clear ();

			GameObject[] temp = GameObject.FindGameObjectsWithTag ("Player");
			foreach (GameObject p in temp) {
				Players.Add (p);
			}
			
	}

	void Awake()
	{
		DontDestroyOnLoad (this);

		//Debug.Log(Rewired.ReInput.controllers.GetCustomControllers().Length);
		TimeToFade = GameObject.FindGameObjectWithTag ("Player").GetComponent<Trail> ().TimeToFade;
		countdownImage = GameObject.FindGameObjectWithTag ("Number");
		Players.Clear ();
		mRef = GameObject.Find ("GameManager").GetComponent<Main> ();
		DeathCount = 0;
		Player1 = GameObject.Find ("Player1");
		//playerPanel1 = GameObject.FindGameObjectWithTag ("p1");
		if (Player1 == null) {
			//playerPanel1.SetActive (false);
		} else {
			Player1Vec = Player1.transform.position;
			Players.Add (Player1);
		}
		

		Player2 = GameObject.Find ("Player2");
		//playerPanel2 = GameObject.FindGameObjectWithTag ("p2");
		if (Player2 == null) {
			//playerPanel2.SetActive (false);
		}else {
			Player2Vec = Player2.transform.position;
			Players.Add (Player2);
		}

		Player3 = GameObject.Find ("Player3");
		//playerPanel3 = GameObject.FindGameObjectWithTag ("p3");
		if (Player3 == null) {
			//playerPanel3.SetActive (false);
		}else {
			Player3Vec = Player3.transform.position;
			Players.Add (Player3);
		}

		Player4 = GameObject.Find ("Player4");
		//playerPanel4 = GameObject.FindGameObjectWithTag ("p4");
		if (Player4 == null) {
			//playerPanel4.SetActive (false);
		}else {
			Player4Vec = Player4.transform.position;
			Players.Add (Player4);
		}
		/*p1Lives = 3;
		p2Lives = 3;
		p3Lives = 3;
		p4Lives = 3;
		*/
	}


	public void CleanUp()
	{
		//Player1.GetComponent<Trail>().CollisionPool

		foreach (GameObject t in Player1.GetComponent<Trail>().CollisionPool) {
			t.gameObject.SetActive (false);
		}
		foreach (GameObject t in Player2.GetComponent<Trail>().CollisionPool) {
			t.gameObject.SetActive (false);
		}
		foreach (GameObject t in Player3.GetComponent<Trail>().CollisionPool) {
			t.gameObject.SetActive (false);
		}
		foreach (GameObject t in Player4.GetComponent<Trail>().CollisionPool) {
			t.gameObject.SetActive (false);
		}

		foreach (TrailRenderer trail in AllLines) {
			trail.enabled = false;
		}

		/*
		Player1.GetComponent<Rigidbody> ().isKinematic = false;
		Player1.GetComponent<PlayerControllerRotation> ().speed = 0;
		Player4.GetComponent<Rigidbody> ().isKinematic = false;
		Player4.GetComponent<PlayerControllerRotation> ().speed = 0;
		Player3.GetComponent<Rigidbody> ().isKinematic = false;
		Player3.GetComponent<PlayerControllerRotation> ().speed = 0;
		Player2.GetComponent<Rigidbody> ().isKinematic = false;
		Player2.GetComponent<PlayerControllerRotation> ().speed = 0;

		mRef.countdownMax = false;
		*/
		foreach (GameObject player in Players) {
			player.GetComponent<Trail> ().NewTrail ();
		}
		/*
		Player1.GetComponent<Trail> ().NewTrail ();
		Player2.GetComponent<Trail> ().NewTrail ();
		Player3.GetComponent<Trail> ().NewTrail ();
		Player4.GetComponent<Trail> ().NewTrail ();
		*/

		Time.timeScale = 0.0f;

	}

	// Update is called once per frame
	void Update () {
		//Debug.Log (GameManager.Instance.countdownMax);
		if (GameManager.Instance.countdownMax && !GameManager.Instance.isGameRunning) {
			GameManager.Instance.isGameRunning = true;
			if (countdownImage != null)
				countdownImage.SetActive (false);
			//StartCoroutine (Delay ());
		}
		//Debug.Log (GameManager.Instance.isGameRunning);
		if (isGameRunning) {
			//Debug.Log (DeathCount);
			//Debug.Log (Players.Count - 1);
			if (DeathCount >= Players.Count - 1) {
				DeathCount = 0;



				StartCoroutine (Delay ());
			}
		}
	}

	private IEnumerator Delay()
	{
		yield return new WaitForSeconds (5);
		isGameRunning = false;
		countdownMax = false;
		isOutOfBounds = false;
		LoadScene ("0_Scene (Nomanager)");
		StopAllCoroutines ();

	}

	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}

}
