﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombPickup : MonoBehaviour {

	public Color[] colors;
	[SerializeField]
	private TRON_COLORS curColor = TRON_COLORS.MAGENTA;

	[SerializeField]
	private SpriteRenderer sprRef;
	[SerializeField]
	private Rigidbody rbRef;

	[SerializeField]
	private Vector3 m_moveTarget;

	public float TimeToSwitch = 4.0f;

	// Use this for initialization
	void Start () {
		sprRef = this.GetComponent<SpriteRenderer> ();
		rbRef = this.GetComponent<Rigidbody> ();
		StartCoroutine (ColorSwitch ());
		colors = new Color[7];
		colors [0] = Color.red;
		colors [1] = new Color32 (255, 120, 12, 255);
		colors [2] = Color.yellow;
		colors [3] = Color.green;
		colors [4] = new Color(202/255, 255/255, 255/255);
		colors [5] = Color.blue;
		colors [6] = new Color32(156, 28, 200, 255);

	}

	// Update is called once per frame
	void Update () {
		rbRef.velocity = m_moveTarget;
	}

	void OnTriggerExit(Collider col)
	{
		if (col.CompareTag ("Arena")) {
			Debug.Log ("Bomb Collision w/ Boundary");
			Vector3 pointOfContact = col.ClosestPointOnBounds(this.transform.position);
			if (pointOfContact.x > 99.0f ||pointOfContact.x < -99.0f) {
				//RIGHT|LEFT SIDE COL
				m_moveTarget = new Vector3(-m_moveTarget.x, m_moveTarget.y, m_moveTarget.z);
			}
			if (pointOfContact.z > 99.0f ||pointOfContact.z < -99.0f) {
				//TOP|BOTTOM SIDE COL
				m_moveTarget = new Vector3(m_moveTarget.x, m_moveTarget.y, -m_moveTarget.z);
			}
		}
	}
	void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag ("Player")) {
			Debug.Log ("Bomb Collision w/ Player");
			GameManager.Instance.bombHolder = col.gameObject;
			Destroy (this.gameObject);
		}
	}

	private IEnumerator ColorSwitch()
	{
		while (true) {
			yield return new WaitForSeconds (TimeToSwitch);
			curColor++;
			if ((int)curColor >= colors.Length)
				curColor = (TRON_COLORS)0;
			sprRef.color = colors [(int)curColor];

		}
	}

}
